import random

def generar_domino():
    domino = []
    for i in range(7):
        for j in range(i, 7):
            domino.append((i, j))
    return domino

def repartir_fichas(domino):
    random.shuffle(domino)
    return domino[:7], domino[7:14], domino[14:]

def jugar_turno(jugador, mesa):
    print(f"Turno del jugador {jugador['nombre']}")
    print("Mesa:", mesa)
    print("Tus fichas:", jugador['fichas'])

    ficha_valida = False
    while not ficha_valida:
        ficha = input("Escribe la ficha que deseas jugar en formato 'a,b' o 'P' para pasar: ")
        if ficha.upper() == 'P':
            return None

        try:
            a, b = map(int, ficha.split(','))
            if (a, b) in jugador['fichas'] or (b, a) in jugador['fichas']:
                ficha_valida = True
            else:
                print("No tienes esa ficha, intenta de nuevo.")
        except ValueError:
            print("Formato incorrecto, intenta de nuevo.")

    return a, b

def actualizar_mesa(mesa, ficha):
    a, b = ficha
    if not mesa:
        return [a, b]

    if a == mesa[0]:
        mesa.insert(0, b)
        mesa.insert(0, a)
    elif a == mesa[-1]:
        mesa.append(a)
        mesa.append(b)
    elif b == mesa[0]:
        mesa.insert(0, a)
        mesa.insert(0, b)
    elif b == mesa[-1]:
        mesa.append(b)
        mesa.append(a)
    return mesa

def jugar_domino():
    domino = generar_domino()
    jugador1_fichas, jugador2_fichas, domino = repartir_fichas(domino)
    jugador1 = {'nombre': 'Jugador 1', 'fichas': jugador1_fichas}
    jugador2 = {'nombre': 'Jugador 2', 'fichas': jugador2_fichas}
    mesa = []

    turno = 0
    while jugador1['fichas'] and jugador2['fichas']:
        jugador_actual = jugador1 if turno % 2 == 0 else jugador2
        ficha = jugar_turno(jugador_actual, mesa)

        if ficha is not None:
            a, b = ficha
            if (a, b) in jugador_actual['fichas']:
                jugador_actual['fichas'].remove((a, b))
            else:
                jugador_actual['fichas'].remove((b, a))
            mesa = actualizar_mesa(mesa, ficha)
        turno += 1

    ganador = 'Jugador 1' if not jugador1['fichas'] else 'Jugador 2'
    print(f"¡{ganador} ha ganado el juego!")

if __name__ == "__main__":
    jugar_domino()